import dotenv from 'dotenv';
import server from './src/server';

dotenv.config();

const port = process.env.PORT || 8000;

server.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}/api`);
});

server.on('error', (error) => {
  console.error(`⚡️ ERROR ⚡️ in server`);
})
