import express, { Express, Request, Response, RouterOptions } from 'express';
import { LogInfo } from '../utils/logger';
import { UsersController } from '../controller/UsersController';

// Router
let userRouter = express.Router();

userRouter.route('/')
    .get(async (req: Request, res: Response) => {
        let name: any = req?.query?.name;
        LogInfo(name);
        const controller: UsersController = new UsersController();
        const response = await controller.getMessage(name);
        return res.send(response);
    });

export default userRouter;