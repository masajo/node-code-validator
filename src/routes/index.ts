import express, { Express, Request, Response, RouterOptions } from 'express';
import helloRouter from './helloRouter';
import userRouter from './userRouter';

// Server
let server = express();

// Router
let rootRouter = express.Router();

rootRouter.get('/', (req: Request, res: Response) => {
    res.send('API Rest: Express + TypeScript + Mongoose + Swagger Server');});

// router.route('/hello')
//     .get((req: Request, res: Response) => {
//         res.json(
//             {
//                 message: '¡Hello world!'
//             }
//         )
//     });

server.use('/', rootRouter);
server.use('/hello', helloRouter);
server.use('/users', userRouter)

export default server;