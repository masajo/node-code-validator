export const SuccessfulResponse = async(status: string, message:string, data: any)=>{
    return await {
        status: status, 
        message: message, 
        data: data
    }
}

export const ErrorResponse = async(status: string, errorCode: string, message:string)=>{
    return await {
        status: status, 
        errorCode: errorCode, 
        message: message
    }
}
