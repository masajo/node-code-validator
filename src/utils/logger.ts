import { CYAN_LOG, GREEN_LOG, RED_LOG, YELLOW_LOG } from './enum';

export const LogSuccess = (message: string) => {
    console.log(GREEN_LOG, message);
}

export const LogInfo = (message: string) => {
    console.log(CYAN_LOG, message);
}

export const LogWarning = (message: string) => {
    console.log(YELLOW_LOG, message);
}

export const LogError = (message: string) => {
    console.log(RED_LOG, message);
}