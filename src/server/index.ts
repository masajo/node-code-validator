import express, { Express, Request, Response, RouterOptions } from 'express';
import dotenv from 'dotenv';

// Logger - Morgan
import morgan from 'morgan'

// Swagger
import swaggerUi from "swagger-ui-express";
import swaggerJSDoc from 'swagger-jsdoc';

// Mongoose
import mongoose from 'mongoose';

// Securization
import cors from 'cors';
import helmet from 'helmet';

// HTTPS
import fs, { appendFile } from 'fs';
import https from 'https';
import router from '../routes';
import swaggerJsdoc from 'swagger-jsdoc';

dotenv.config();

const server: Express = express();

// * Settings and Options


// * Middlewares + Router + Swagger
server.use(
    "/docs",
    swaggerUi.serve,
    swaggerUi.setup(undefined, {
      swaggerOptions: {
        url: "/swagger.json",
        explorer: true
      },
    })
);

server.use(
    '/api', 
    router
);

// Logger Middleware
server.use(morgan('tiny'));

// Static Serve
server.use(express.static('public'));

// * Mongoose Connection


// * Secure
server.use(helmet());
server.use(cors());

// * Content type
server.use(express.urlencoded({ extended: true, limit: '50mb' }));
server.use(express.json({limit: '50mb'}));

// * Routes: Redirecto to BaseURL
server.get('/', (req: Request, res: Response) => {
    res.redirect('/api');
});

// https.createServer({
//   cert: fs.readFileSync('cert.pem'),
//   key: fs.readFileSync('key.pem')
// }, app).listen(
    //   port, () => console.log(`⚡️[server]: Server is running at http://localhost:${port}`)
// );

// Capture ERROR 404 Configuration
// server.use(function(req: Request, res: Response, next) {
//   var err:any = new Error('Not Found');
//   err.status = 404;
//   next(err);
// });

// Capture ERROR 500 Configuration
// server.use(function(err: any, req: Request, res: Response, next: () => void) {
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};
//   res.status(err.status || 500);
//   res.render('error');
// });
        
export default server;