import { Request, Response } from "express";
import { BasicResponse } from "../types";
import { SuccessfulResponse, ErrorResponse } from "@/utils/responses";

export interface iHelloController {
    getMessage(name?: string): Promise<BasicResponse>
}

export interface iUsersController {
    getMessage(name?: string): Promise<any>
}