import { Get, Path, Query, Route, SuccessResponse, Tags } from "tsoa";
import { BasicResponse } from "./types";
import { iHelloController } from './interfaces';
import { LogSuccess } from "../utils/logger";

@Route("/api/hello")
@Tags("HelloController")
export class HelloController implements iHelloController {
  /**
    * Retrieves a greeting message "Hello World" in JSON
  */
  @Get("/")
  public async getMessage(@Query() name?:string): Promise<BasicResponse> {
    LogSuccess('[[/api/hello]] HelloWorld')

    return {
      message: `Hello, ${name || "World"}!`,
    };
  }
}