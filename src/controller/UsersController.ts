import { Get, Query, Route, Tags } from "tsoa";
import { Request, Response } from "express";
import { iUsersController } from './interfaces';
import { LogSuccess } from "../utils/logger";
import { GetAll } from '@/domain/services/userService';

@Route("/api/users")
@Tags("UsersController")
export class UsersController implements iUsersController {
  /**
    * Retrieves a greeting message "User, <name>!" to User in JSON
  */
  @Get("/")
  public async getMessage(@Query() name?:string, ): Promise<any> {
    LogSuccess('[[/api/users]] Users')
    // FIXME: Error in calling Service
    // return GetAll();
    return (`Hello, ${name}`)
  }
}