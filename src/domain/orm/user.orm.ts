import { userEntity } from "../entities/user.entity"

import { uuid } from "uuidv4" 
import { LogError } from "@/utils/logger";

export const GetAllUsers = async () => {
    try {
        let userModel = userEntity();
        return await userModel.find({isDelete: false})
    } catch (error) {
        LogError(`Error ORM Get All Users: ${error}`);
    }
}

export const GetUserByID = async (id: string) => {
    
}

export const CreateUser = async (user: any) => {
    
}

export const DeleteUserByID = async (id: string) => {
    
}

export const UpdateUserByID = async (id: string, user: any) => {
    
}