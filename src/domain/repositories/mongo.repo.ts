import mongoose from "mongoose";
import dotenv from 'dotenv';
import { CYAN_LOG, GREEN_LOG, RED_LOG, YELLOW_LOG } from '@/utils/enum';
import { LogSuccess } from "@/utils/logger";

dotenv.config();

const hostDB = process.env.HOSTDB || 'localhost';
const portDB = process.env.PORTDB || '27017';
const userNameDB = process.env.USERNAME || 'test';
const passwordDB = process.env.PASSWORD || '123456';
const dbName = process.env.DATABASE || 'demo';
const dbConnName = process.env.NAMECONNDB || 'connMongo';

// mongoose.connect(`mongodb://${hostDB}:${portDB}/${dbName}`);