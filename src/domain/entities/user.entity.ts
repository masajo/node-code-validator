import mongoose from "mongoose";

export const userEntity = () => {
    
    let userSchema = new mongoose.Schema({
        name: String,
        lastName: String,
        email: String,
        password: String
    });

    return mongoose.model('Users', userSchema);
}