import { Request, Response } from "express";
import {} from '@/utils/logger';
import { CODE_BAD_REQUEST, CODE_INTERNAL_SERVER_ERROR, CODE_NO_CONTENT, CODE_OK, CRASH_LOGIC } from '@/utils/enum';
import { GetAllUsers } from '@/domain/orm/user.orm';
import { isUuid } from 'uuidv4';
import { ErrorResponse, SuccessfulResponse } from '@/utils/responses';

export const GetAll = async (req: Request, res: Response) => {

    let status:string = 'Success';
    let errorCode:string = '';
    let message:string = '';
    let data:string= '';
    let statusCode:number = 0;
    let response = {};

    try {
        let responseORM: any = await GetAllUsers()
        if(responseORM.err){
            status = 'Failure';
            errorCode = responseORM.err.code;
            message = responseORM.err.message;
            statusCode = CODE_BAD_REQUEST;
        }else {
            message = 'Success Response', 
            data = responseORM, 
            statusCode = data.length > 0 ? CODE_OK : CODE_NO_CONTENT;
        }
        response = await SuccessfulResponse(status,message,data);
        return res.status(statusCode).send(response);
    } catch (error) {
        console.log("err = ", error);
        response = await ErrorResponse('Failure',CRASH_LOGIC,'Error in server searching for all users');
        return res.status(CODE_INTERNAL_SERVER_ERROR).send(response);
    }
}

// TODO: GetBYID, Create, Delete & Update